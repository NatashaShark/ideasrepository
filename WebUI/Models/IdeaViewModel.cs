﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class IdeaViewModel
    {
        public Idea Idea { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public Author Author {get; set;}
    }
}