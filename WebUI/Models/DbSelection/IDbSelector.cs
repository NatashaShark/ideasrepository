﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebUI.Models.DbSelection
{
    public interface IDbSelector
    {
       IdeasListViewModel GetIdeasList(IIdeasRepository repository, int page, int pageSize);
       IdeasListViewModel GetIdeasTrashList(IIdeasRepository repository, int page, int pageSize);
       CommentsListViewModel GetCommentsList(IIdeasRepository repository, int page, int pageSize);
       Author GetUser(IIdeasRepository repository, int userId);
       Idea GetIdea(IIdeasRepository repository, int ideaId);
    }
}
