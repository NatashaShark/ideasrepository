﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.DbSelection
{
    public class EFIdeasRepositorySelector : IDbSelector
    {
        public IdeasListViewModel GetIdeasList(Domain.Abstract.IIdeasRepository repository, int page, int pageSize)
        {
            IdeasListViewModel model;
            int totalPagesCount = 0;
            if (repository.ActiveIdeas != null)
            {
                totalPagesCount = repository.ActiveIdeas.Count();
            }
            IQueryable<Idea> ideas = GetIdeasPage(repository.ActiveIdeas, repository, page, pageSize);
            if (ideas != null)
            {
                model = new IdeasListViewModel
                {
                    Ideas = ideas,
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = pageSize,
                        TotalItems = totalPagesCount
                    }
                };
            }
            else
            {
                model = new IdeasListViewModel
                {
                    Ideas = new List<Idea>(),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = 1,
                        ItemsPerPage = 1,
                        TotalItems = 1
                    }
                };
            }

            return model; 
        }

        public IdeasListViewModel GetIdeasTrashList(IIdeasRepository repository, int page, int pageSize)
        {
            IdeasListViewModel model;
            int totalPagesCount = 0;
            if (repository.NonActiveIdeas != null)
            {
                totalPagesCount = repository.NonActiveIdeas.Count();
            }
            IQueryable<Idea> ideas = GetIdeasPage(repository.NonActiveIdeas, repository, page, pageSize);
            if (ideas != null)
            {
                model = new IdeasListViewModel
                {
                    Ideas = ideas,
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = pageSize,
                        TotalItems = totalPagesCount
                    }
                };
            }
            else
            {
                model = new IdeasListViewModel
                {
                    Ideas = new List<Idea>(),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = 1,
                        ItemsPerPage = 1,
                        TotalItems = 1
                    }
                };
            }

            return model;
        }

        private IQueryable<Idea> GetIdeasPage(IQueryable<Idea> ideasList,IIdeasRepository repository, int page, int pageSize)
        {
            IQueryable<Idea> ideas = null;
            if (ideasList != null)
            {
                ideas = ideasList
                 .OrderBy(i => i.Date)
                 .Skip((page - 1) * pageSize)
                 .Take(pageSize);
                foreach (var idea in ideas.ToList())
                {
                    var comments = (from c in repository.Comments
                                    where c.IdeaId == idea.IdeaId
                                    select c).ToList();
                    idea.Comments = comments;
                }
                foreach (var idea in ideas.ToList())
                {
                    idea.Author = (from a in repository.Authors
                                   where a.UserId == idea.UserId
                                   select a).ToList()[0];
                }
            }

            return ideas;
        }

        public CommentsListViewModel GetCommentsList(IIdeasRepository repository, int page, int pageSize)
        {
            var comments = repository.Comments
                 .OrderBy(i => i.Date)
                 .Skip((page - 1) * pageSize)
                 .Take(pageSize);
            foreach (var comment in comments.ToList())
            {
                var author = (from a in repository.Authors
                              where a.UserId == comment.UserId
                              select a).ToList()[0];

                Idea idea = (from i in repository.ActiveIdeas
                             where i.IdeaId == comment.IdeaId
                             select i).ToList()[0];
                comment.Author = author;
                comment.Idea = idea;
            }

            CommentsListViewModel model = new CommentsListViewModel
            {
                Comments = comments,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = repository.ActiveIdeas.Count()
                }
            };

            return model;
        }

        public Domain.Entities.Author GetUser(IIdeasRepository repository, int userId)
        {
            throw new NotImplementedException();
        }

        public Domain.Entities.Idea GetIdea(IIdeasRepository repository, int ideaId)
        {
            Idea idea = repository.ActiveIdeas
             .FirstOrDefault(i => i.IdeaId == ideaId);
            var author = (from a in repository.Authors
                          where a.UserId == idea.UserId
                          select a).ToList()[0];
            idea.Author = author;
            List<Comment> comments = (from c in repository.Comments
                                      where c.IdeaId == ideaId
                                      select c).ToList();
            idea.Comments = comments;

            return idea;
        }
    }
}