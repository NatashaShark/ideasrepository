﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class CommentsListViewModel
    {
        public IEnumerable<Comment> Comments { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}