﻿using Domain.Abstract;
using Domain.Concrete;
using Moq;
using Ninject;
using Ninject.Web.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebUI.Auth;
using WebUI.Models.DbSelection;

namespace WebUI.Infrastructure
{
        public class NinjectControllerFactory : DefaultControllerFactory
        {
            private IKernel ninjectKernel;
            public NinjectControllerFactory()
            {
                ninjectKernel = new StandardKernel();
                AddBindings();
            }
            protected override IController GetControllerInstance(RequestContext requestContext,
           Type controllerType)
            {
                return controllerType == null
                ? null
                : (IController)ninjectKernel.Get(controllerType);
            }
            private void AddBindings()
            {
                ninjectKernel.Bind<IIdeasRepository>().To<EFIdeasRepository>();
                ninjectKernel.Bind<IAuthentification>().To<AuthenticatifionManager>().InRequestScope();
                ninjectKernel.Bind<IDbSelector>().To<EFIdeasRepositorySelector>();
            }
        }
}