﻿using Domain.Abstract;
using Domain.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using WebUI.Auth;
using WebUI.Models;
using WebUI.Models.Ausorization;
using WebUI.Models.DbSelection;

namespace WebUI.Controllers
{
    [ModeratorAuthorize (Role.Administrator)]
    public class UserController : Controller
    {
        [Inject]
        private IIdeasRepository Repository { get; set; }
        [Inject]
        public IAuthentification Auth { get; set; }
        [Inject]
        public IDbSelector DbSelector { get; set; }
        public int PageSize = 20;

        public UserController(IIdeasRepository ideasRepository, IAuthentification auth, IDbSelector dbSelector)
         {
            this.Repository = ideasRepository;
            this.Auth = auth;
            this.DbSelector = dbSelector;
         }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.Author = CurrentAuthor;
        }

        public LoggedinAuthor CurrentAuthor
        {
            get
            {
                Auth.HttpContext = this.HttpContext.ApplicationInstance.Context;
                var currentUser = Auth.CurrentUser;
                if (currentUser != null)
                {
                    return ((UserIndentity)currentUser.Identity).User.ToLoggedinAuthor();
                }
                return null;
            }
        }
      
        [HttpGet]
        public ActionResult IdeasList(int page = 1)
         {
             IdeasListViewModel model = DbSelector.GetIdeasList(Repository, page, PageSize); 
            
             return View(model);
         }
        
        [HttpGet]
        public ActionResult UserPage(int userId)
         {
             Author author = Repository.GetAuthor(userId);

             return View(author);
         }

        [HttpGet]
        public ActionResult EditIdea(int ideaId)
         {
             Idea idea = DbSelector.GetIdea(Repository, ideaId);
             
             return View(idea);
         }

         [HttpPost]
         public ActionResult EditIdea(Idea idea)
         {
             if (idea.UserId == CurrentAuthor.UserId)
             {
                 if (ModelState.IsValid)
                 {
                     Repository.SaveIdea(idea);
                     TempData["message"] = string.Format("{0} has been saved", idea.Title);
                     return RedirectToAction("IdeasList");
                 }
                 else
                 {
                     return View(idea);
                 }
             }

             return RedirectToAction("IdeasList");
         }
        [HttpGet]
         public ActionResult CreateIdea()
         {
             return View("EditIdea", new Idea { UserId = CurrentAuthor.UserId, Date = DateTime.Now, Status = IdeaStatus.IS_ACTIVE});
         }
        [HttpGet]
        public ActionResult MoveIdeaToTrash(int ideaId)
         {
             Repository.MoveIdeaToTrash(ideaId);

             return RedirectToAction("IdeasList");
         }

        [HttpGet]
         public ActionResult DeleteComment(int commentId)
         {
             Repository.DeliteComment(commentId);

             return RedirectToAction("IdeasList");
         }
        [HttpGet]
         public ActionResult GetIdeaFromTrash(int ideaId)
         {
             Repository.GetIdeaFromTrash(ideaId);

             return RedirectToAction("Trash");
         }

        [HttpGet]
        public ActionResult DeleteIdea(int ideaId)
        {
            Repository.DeliteIdea(ideaId);

            return RedirectToAction("Trash");
        }
        [HttpGet]
         public ViewResult Trash(int page = 1)
         {
             IdeasListViewModel model = DbSelector.GetIdeasTrashList(Repository, page, PageSize);
             
             return View(model);
         }


        [HttpGet]
        public ViewResult CommentsList(int page = 1)
         {
             CommentsListViewModel model = DbSelector.GetCommentsList(Repository, page, PageSize);
             
             return View(model);
         }
    }
}
