﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using WebUI.Models;
using Domain.Entities;
using WebUI.Models.DbSelection;
using Ninject;
using WebUI.Auth;

namespace WebUI.Controllers
{
    public class IdeasController : Controller
    {
        [Inject]
         private IIdeasRepository repository;
        [Inject]
        IAuthentification auth;
        [Inject]
        private IDbSelector dbSelector;
         private int PageSize = 5;

         public IdeasController(IIdeasRepository ideasRepository, IAuthentification auth, IDbSelector dbSelector)
         {
             this.repository = ideasRepository;
             this.auth = auth;
             this.dbSelector = dbSelector;
         }

         protected override void Initialize(System.Web.Routing.RequestContext requestContext)
         {
             base.Initialize(requestContext);
             ViewBag.Author = CurrentAuthor;
         }

         public LoggedinAuthor CurrentAuthor
         {
             get
             {
                 auth.HttpContext = this.HttpContext.ApplicationInstance.Context;
                 var currentUser = auth.CurrentUser;
                 if (currentUser != null)
                 {
                     return ((UserIndentity)currentUser.Identity).User.ToLoggedinAuthor();
                 }
                 return null;
             }
         }

         public ViewResult IdeasList(int page = 1)
         {
             IdeasListViewModel model = dbSelector.GetIdeasList(repository, page, PageSize);
             return View(model);
         }

         public ViewResult IdeaPage(int ideaId)
         {
             Idea idea = dbSelector.GetIdea(repository, ideaId);

             return View(idea);
         }
         [HttpGet]
         public ViewResult UserPage(string login)
         {
             Author author = repository.GetAuthor(login);
             return View(author);
         }

         [HttpGet]
         public ActionResult MoveIdeaToTrash(int ideaId)
         {
             repository.MoveIdeaToTrash(ideaId);

             return RedirectToAction("IdeasList");
         }

         [HttpGet]
         public ActionResult CreateIdea()
         {
             return View("EditIdea", new Idea { UserId = CurrentAuthor.UserId, Date = DateTime.Now, Status = IdeaStatus.IS_ACTIVE });
         }

         [HttpGet]
         public ActionResult EditIdea(int ideaId)
         {
             Idea idea = dbSelector.GetIdea(repository, ideaId);

             return View(idea);
         }

         [HttpPost]
         public ActionResult EditIdea(Idea idea)
         {
             if (idea.UserId == CurrentAuthor.UserId)
             {
                 if (ModelState.IsValid)
                 {
                     repository.SaveIdea(idea);
                     return RedirectToAction("IdeasList");
                 }
                 else
                 {
                     return View(idea);
                 }
             }
             return RedirectToAction("IdeasList");
         }
    }
}
