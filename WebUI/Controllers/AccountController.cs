﻿using Domain.Abstract;
using Domain.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using WebUI.Auth;
using WebUI.Models.Ausorization;

namespace WebUI.Controllers
{
    public class AccountController : Controller 
    {

        private string cookieName = FormsAuthentication.FormsCookieName;

        [Inject]
        private IAuthentification Auth { get; set; }


        public AccountController(IAuthentification auth)
         {
            this.Auth = auth;
         }
        [HttpGet]
        public ActionResult LogOut()
        {
            Auth.HttpContext = System.Web.HttpContext.Current; 
            Auth.LogOut();
            return RedirectToAction("IdeasList", "Ideas");
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View(new LoginView());
        }
        [HttpPost]
        public ActionResult Login(LoginView loginView)
        {
            if (ModelState.IsValid)
            {
                Auth.HttpContext = System.Web.HttpContext.Current;                
                bool isAuthorized = Auth.Login(loginView.Login, loginView.Password, loginView.IsPersistent);
                if (isAuthorized)
                {
                    if (Auth.CurrentUser.IsInRole(Role.Administrator))
                    {
                        return RedirectToAction("IdeasList", "User");
                    }
                    return RedirectToAction("IdeasList", "Ideas");
                }
                ModelState["Password"].Errors.Add("Invalid login or password");
            }                
            return View();

        }

        [HttpGet]
        public ActionResult CheckIn()
        {
            return View(new CheckInView());
        }
        [HttpPost]
        
        public ActionResult CheckIn(CheckInView checkInView)
        {
            if (ModelState.IsValid)
            {
                Auth.HttpContext = System.Web.HttpContext.Current;
                bool isUnique = Auth.IsUnique(checkInView.Login);
                if (isUnique)
                {
                    Author newUser = Auth.CheckIn(
                        checkInView.Login,
                        checkInView.Password,
                        checkInView.Name,
                        checkInView.Surname,
                        checkInView.Email,
                        checkInView.City);
                    return RedirectToAction("UserPage", "Ideas", new { login = checkInView.Login});
                }
                else {
                    ModelState["Login"].Errors.Add("This login already exist");
                }
            }
            return View(checkInView);

        }
    }
}
