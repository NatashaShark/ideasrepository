﻿using Domain.Abstract;
using Domain.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;

namespace WebUI.Auth
{
    public class AuthenticatifionManager : IAuthentification
    {

        private const string cookieName = "__AUTH_COOKIE";

        public HttpContext HttpContext { get; set; }

        [Inject]
        public IIdeasRepository Repository { get; set; }

        public AuthenticatifionManager()
        {
            this.Repository = (IIdeasRepository)DependencyResolver.Current.GetService(typeof(IIdeasRepository));
        }


        public bool Login(string userName, string password, bool isPersistent)
        {
            if (CurrentUser == null)
            {
                var retUser = Repository.Login(userName, password);
                if (retUser != null)
                {
                    CreateCookie(userName, isPersistent);
                    return true;
                }
                return false;
            }
            return true;
        }

        public bool Login(string userName)
        {
            var retUser = Repository.Users.FirstOrDefault(p => string.Compare(p.Login, userName, true) == 0);
            if (retUser != null)
            {
                if (CreateCookie(userName))
                {
                    return true;
                }                
            }
            return false;
        }

        private bool CreateCookie(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                  1,
                  userName,
                  DateTime.Now,
                  DateTime.Now.Add(FormsAuthentication.Timeout),
                  isPersistent,
                  string.Empty,
                  FormsAuthentication.FormsCookiePath);

            var encTicket = FormsAuthentication.Encrypt(ticket);

            /*if (HttpContext.Request.Cookies[cookieName] != null)
            {
                HttpContext.Response.Cookies[cookieName].Value = encTicket;
                HttpContext.Response.Cookies[cookieName].Expires = DateTime.Now.Add(FormsAuthentication.Timeout);
            }
            else
            {
                var AuthCookie = new HttpCookie(cookieName)
                {
                    Value = encTicket,
                    Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
                };
                HttpContext.Response.Cookies.Add(AuthCookie);
            }*/
            var AuthCookie = new HttpCookie(cookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            HttpContext.Response.Cookies.Set(AuthCookie);

            var createdCookie = HttpContext.Response.Cookies[cookieName];

            bool isCreated = createdCookie != null && !string.IsNullOrEmpty(createdCookie.Value);
            return isCreated;
        }

        public void LogOut()
        {
            var httpCookie = HttpContext.Response.Cookies[cookieName];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
                httpCookie.Expires = DateTime.Now.AddDays(-1);
            }
        }

        private IPrincipal _currentUser;

        public IPrincipal CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    try
                    {
                        HttpCookie authCookie = HttpContext.Request.Cookies.Get(cookieName);
                        if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
                        {
                            var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                            _currentUser = new UserProvider(ticket.Name, Repository);
                        }
                        else
                        {
                            _currentUser = null;
                        }
                    }
                    catch (Exception)
                    {
                        _currentUser = null;
                    }
                }
                return _currentUser;
            }
        }
        
        public bool IsUnique(string login)
        {
            bool isUnique = Repository.IsUnique(login);
            return isUnique;
        }

        public Author CheckIn(string login, string password, string name, string surname, string email, string city)
        {
            Repository.CheckIn(login, password, name, surname, email, city);
            Author author = Repository.GetAuthor(login);
            return author;
        }
    }
}