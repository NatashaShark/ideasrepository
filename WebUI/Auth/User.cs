﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Auth
{
    public class User
    {
        public string Login { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int UserId { get; set; }

        public string City { get; set; }
        public string Surname { get; set; }

        public string Role { get; set; }

        public LoggedinAuthor ToLoggedinAuthor()
        {
            LoggedinAuthor author = new LoggedinAuthor()
            {
                Login = this.Login,
                UserId = this.UserId,
                Name = this.Name,
                Surname = this.Surname,
                Email = this.Email,
                City = this.City
            };
            return author;
        }
        public bool InRoles(string roles)
        {
            if (string.IsNullOrWhiteSpace(roles))
            {
                return false;
            }

            var rolesArray = roles.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var role in rolesArray)
            {
                var hasRole = Role.Equals(role);
                if (hasRole)
                {
                    return true;
                }
            }
            return false;
        }
    }
}