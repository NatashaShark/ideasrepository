﻿using Domain.Abstract;
using Domain.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace WebUI.Auth
{
    public class UserIndentity : IIdentity, IUserProvider
    {
        public User User { get; set; }

        [Inject]
        public IAuthentification Auth { get; set; }

        public User CurrentUser
        {
            get
            {
                return ((IUserProvider)Auth.CurrentUser.Identity).User;
            }
        }
        public string AuthenticationType
        {
            get
            {
                return typeof(User).ToString();
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return User != null;
            }
        }

        public string Name
        {
            get
            {
                if (User != null)
                {
                    return User.Login;
                }
                return "unknown";
            }
        }

        public void Init(string login, IIdeasRepository repository)
        {
            if (!string.IsNullOrEmpty(login))
            {
                Author author = repository.GetAuthor(login);
                Domain.Entities.User user = repository.GetUser(login);
                User = new User
                { 
                    Login = login, 
                    UserId = author.UserId,
                    Name = author.Name,
                    Surname = author.Surname,
                    City = author.City,
                    Email = author.Email,
                    Role = user.Type
                };
            }
        }
    }
}