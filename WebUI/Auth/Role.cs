﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Auth
{
    public static class Role
    {
        public const string Administrator = "admin";
        public const string Moderator = "moderator";
    }
}