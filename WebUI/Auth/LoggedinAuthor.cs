﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Auth
{
    public class LoggedinAuthor : Author
    {
        public string Login { get; set; }
    }
}