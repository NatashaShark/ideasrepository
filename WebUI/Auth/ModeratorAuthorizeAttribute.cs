﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI.Auth
{
    public class ModeratorAuthorizeAttribute : AuthorizeAttribute
    {
        [Inject]
        IAuthentification Auth { get; set; }

        public ModeratorAuthorizeAttribute(IAuthentification auth)
        {
            Auth = auth;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContextBase)
        {
            Auth = new AuthenticatifionManager();
            //IKernel ninjectKernel = new StandardKernel();
            //ninjectKernel.Bind<IAuthentification>().To<AuthenticatifionManager>();
            //Auth = ninjectKernel.TryGet<IAuthentification>();
            //Auth = (IAuthentification)DependencyResolver.Current.GetService<IAuthentification>();
            Auth.HttpContext = httpContextBase.ApplicationInstance.Context;
            if (Auth.CurrentUser == null)
            {
                return false;
            }

            string authenticatedUser = Auth.HttpContext.User.Identity.Name;

            if (!this.IsProfileCompleted(authenticatedUser))
            {
                Auth.HttpContext.Items["redirectToCompleteProfile"] = true;
                return false;
            }

            return true;
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Items.Contains("redirectToCompleteProfile"))
            {
                var routeValues = new RouteValueDictionary(new
                {
                    controller = "Ideas",
                    action = "IdeasList",
                });
                filterContext.Result = new RedirectToRouteResult(routeValues);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

        private bool IsProfileCompleted(string user)
        {
            return ((UserIndentity)Auth.CurrentUser.Identity).User.Login.Equals(user);
        }
    }
}