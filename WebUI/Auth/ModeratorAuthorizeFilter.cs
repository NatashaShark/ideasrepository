﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI.Auth
{
    
    public class ModeratorAuthorizeAttribute : AuthorizeAttribute
    {
        [Inject]
        IAuthentification Auth { get; set; }

        public ModeratorAuthorizeAttribute() : base()
        {
            Auth = (IAuthentification)DependencyResolver.Current.GetService(typeof(IAuthentification));
        }

        public ModeratorAuthorizeAttribute(params string[] roles)
            : this()
        {
            Roles = string.Join(",", roles);
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            Auth.HttpContext = httpContext.ApplicationInstance.Context;
            var authenticatedUser = Auth.CurrentUser;

            if (authenticatedUser == null)
            {
                httpContext.Items["redirectToCompleteProfile"] = true;
                return false;
            }

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Items.Contains("redirectToCompleteProfile"))
            {
                var routeValues = new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login",
                });
                filterContext.Result = new RedirectToRouteResult(routeValues);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

    }
    
}