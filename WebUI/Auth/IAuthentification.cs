﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebUI.Auth
{
    public interface IAuthentification
    {
        HttpContext HttpContext { get; set; }
        bool Login(string login, string password, bool isPersistent);
        void LogOut();
        IPrincipal CurrentUser { get; }

        bool IsUnique(string login);

        Domain.Entities.Author CheckIn(string login,string password, string name, string surname, string email, string city);
    }
}
