﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Data.Entity;
using Domain.Abstract;
using System.Data.SqlClient;

namespace Domain.Concrete
{
    public class EFIdeasRepository : IIdeasRepository
    {
        private EFDbContext context = new EFDbContext();
        
        public IQueryable<Idea> ActiveIdeas
        {
            get 
            {
                var i = context.Ideas.SqlQuery("SELECT * FROM Ideas WHERE Status = 1");
                return i.AsQueryable<Idea>(); 
                //return context.Ideas.Where(i => i.Status == IdeaStatus.IS_ACTIVE); 
            }
        }

        public IQueryable<Idea> NonActiveIdeas
        {
            get
            {
                var i = context.Ideas.SqlQuery("SELECT * FROM Ideas WHERE Status = 0");
                return  i.AsQueryable<Idea>(); 
                    //context.Ideas.Where(i => i.Status == IdeaStatus.IS_IN_TRASH); 
            }
        }


        public IQueryable<Comment> Comments
        {
            get { return context.Comments; }
        }

        public IQueryable<Author> Authors
        {
            get { return context.Authors; }
        }


        public void SaveIdea(Idea idea)
        {
            if (idea.IdeaId == 0)
             {
                 context.Ideas.Add(idea);
             }
             else
             {
                 Idea dbEntry = context.Ideas.Find(idea.IdeaId);
                 if (dbEntry != null)
                 {
                     dbEntry.Title = idea.Title;
                     dbEntry.Context = idea.Context;
                     dbEntry.Date = idea.Date;
                     dbEntry.UserId = idea.UserId;
                     dbEntry.Status = IdeaStatus.IS_ACTIVE;
                 }
             }
             context.SaveChanges();
        }


        public bool DeliteIdea(int ideaId)
        {
            bool done = false;
            Idea idea = context.Ideas.Find(ideaId);
            if (idea != null)
            {
                if (idea.Status == IdeaStatus.IS_ACTIVE)
                {
                    done = MoveIdeaToTrash(idea.IdeaId);
                }
                else
                {
                    context.Ideas.Remove(idea);
                    context.SaveChanges();
                    done = true;
                }                
            }
            return done;
        }

        public bool MoveIdeaToTrash(int ideaId)
        {
            bool done = false;
            Idea idea = context.Ideas.Find(ideaId);
            if (idea != null)
            {
                idea.Status = IdeaStatus.IS_IN_TRASH;
                context.SaveChanges();
                done = true;
            }
            return done;
        }

        public bool GetIdeaFromTrash(int ideaId)
        {
            bool done = false;
            Idea idea = context.Ideas.Find(ideaId);
            if (idea != null)
            {
                idea.Status = IdeaStatus.IS_ACTIVE;
                context.SaveChanges();
                done = true;
            }
            return done;
        }


        public IQueryable<User> Users
        {
            get { return context.Users; }
        }

        public bool SaveUser(User user, Author author)
        {
            if (user.UserId == 0)
            {
                context.Users.Add(user);
                context.SaveChanges();
                author.UserId = user.UserId;
                context.Authors.Add(author);
            }
            else
            {
                User dbEntry = context.Users.Find(user.UserId);
                if (dbEntry != null)
                {
                    dbEntry.Login = user.Login;
                    dbEntry.Password = user.Password;

                    Author dbAuthor = context.Authors.Find(user.UserId);
                    dbAuthor.Name = author.Name;
                    dbAuthor.Surname = author.Surname;
                    dbAuthor.Email = author.Email;
                    dbAuthor.City = author.City;
                }
            }
            context.SaveChanges();
            return true;
        }


        public Author GetAuthor(int userId)
        {
            return context.Authors.Find(userId);
        }

        public Author GetAuthor(string userLogin)
        {
            int userId = context.Users.FirstOrDefault(u => u.Login.Equals(userLogin)).UserId;
            return GetAuthor(userId);
        }


        public User Login(string login, string password)
        {
            User user = context.Users.FirstOrDefault(u => u.Login.Equals(login) && u.Password.Equals(password));
            return user;
        }

        public bool HasUser (int userId)
        {
            User user = context.Users.Find(userId);
            return user != null;
        }


        public void DeliteComment(int commentId)
        {
            Comment comment = context.Comments.Find(commentId);
            if (comment != null)
            {
                context.Comments.Remove(comment);
                context.SaveChanges();
            }
        }

        public IQueryable<Idea> AllIdeas
        {
            get { return context.Ideas; }
        }


        public User GetUser(string login)
        {
            return context.Users.FirstOrDefault(u => u.Login.Equals(login));
        }


        public void CheckIn(string login, string password, string name, string surname, string email, string city)
        {
            User newUser = new User()
            {
                Login = login,
                Password = password,
                Type = Role.Moderator
            };
            context.Users.Add(newUser);
            context.SaveChanges();
            int userId = context.Users.FirstOrDefault(u => u.Login.Equals(login)).UserId;
            Author newAuthor = new Author()
            {
                UserId = userId,
                Name = name,
                Surname = surname,
                Email = email,
                City = city
            };
            context.Authors.Add(newAuthor);

            context.SaveChanges();
        }

        public bool IsUnique(string login)
        {
            var existentUser = context.Users.FirstOrDefault(u => u.Login.Equals(login));
            return existentUser == null ? true : false;
        }
    }
}
