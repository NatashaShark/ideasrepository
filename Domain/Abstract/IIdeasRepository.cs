﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IIdeasRepository
    {
        IQueryable<Idea> AllIdeas { get; }
        IQueryable<Idea> ActiveIdeas { get; }
        IQueryable<Idea> NonActiveIdeas { get; }
        IQueryable<Comment> Comments { get; }
        IQueryable<Author> Authors { get; }
        IQueryable<User> Users { get; }

        bool SaveUser(User user, Author author);
        void SaveIdea(Idea idea);
        bool DeliteIdea(int ideaId);
        bool MoveIdeaToTrash(int ideaId);
        bool GetIdeaFromTrash(int ideaId);

        Author GetAuthor(int UserIduserId);
        Author GetAuthor(string userLogin);
        User Login(string login, string password);
        User GetUser(string login);
        
        bool HasUser(int userId);

        void DeliteComment(int commentId);

        void CheckIn(string login, string password, string name, string surname, string email, string city);

        bool IsUnique(string login);
    }
}
