﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Entities
{
    public class Idea
    {
        [HiddenInput(DisplayValue = false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdeaId { get; set; }
        [Required(ErrorMessage = "Please enter a idea title")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please enter a description")]
        public string Context { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime Date { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int UserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }
        [NotMapped]
        public virtual ICollection<Comment> Comments { get; set; }
        [NotMapped]
        public virtual Author Author { get; set; }

    }
}
