﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public static class IdeaStatus
    {
        public const int IS_IN_TRASH = 0;
        public const int IS_ACTIVE = 1;
    }
}
