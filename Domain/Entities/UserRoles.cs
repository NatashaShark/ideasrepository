﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.Ausorization
{
    public static class UserRoles
    {
        private static const string[] roles = { "admin", "moderator" };
        static string [] Roles 
        {
            get 
            {
                return roles;
            }
        }
    }
}