﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }
        public string Context { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public int IdeaId { get; set; }
        [NotMapped]
        public virtual Idea Idea { get; set; }
        [NotMapped]
        public virtual Author Author { get; set; }
    }
}
